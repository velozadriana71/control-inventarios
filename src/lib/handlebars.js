const { format } = require('timeago.js');


const helpers = {};
helpers.timeago = (timestamp) => {
    return format(timestamp);
};

helpers.registerHelper = ('Nav', (a, b) => {
     return a == b ? true : false;
 });


// Handlebars.registerHelper('Nav', (a, b, opts) => {
//     return a == b ? opts.fn(this) : opts.inverse(this);
// });

module.exports = helpers;