
const { request } = require('express');
const express = require('express');
const router = express.Router();

const pool = require('../database');
const {isLoggedIn} = require ('../lib/auth');



//VISTA categoria/add

router.get('/add', isLoggedIn, (req,res) => {
      res.render('categorias/add');
  });

  router.post('/add', isLoggedIn, async (req,res) => {
    const {  nombre_categoria, descripcion_categoria, date_added } = req.body;
    //date_added = date("Y-m-d H:i:s");
    const newCategoria = {
        nombre_categoria,
        descripcion_categoria,
        date_added,
        //id_categoria: req.user.id
    };
    await pool.query('INSERT INTO categorias SET ?', [newCategoria]);
    req.flash('success','Guardador Correctamente');
    res.redirect('/categoria');
});

//VISTA /categoria

 router.get('/',  async (req, res) => {
     const categoria = await pool.query('SELECT * FROM categorias');
     //date_added = date("Y-m-d H:i:s");
     console.log(categoria);
     res.render('categorias/list.categorias.hbs', {categoria});
 });


// router.get('/', isLoggedIn,  async (req, res) => {
//     const categoria = await pool.query('SELECT * FROM categorias WHERE id_categoria =?', [req.user.id]);
//     //console.log (links);
//     res.render('categorias/list.categorias.hbs', { categoria });
// });


// VISTA categoria/edit/:id

router.get('/edit/:id', isLoggedIn, async (req, res) => {
    const { id } = req.params;
    const categoria = await pool.query('SELECT * FROM categorias WHERE id_categoria = ?', [id]);
    res.render('categorias/edit.categorias.hbs', {categoria: categoria[0]});
});

router.post('/edit/:id', isLoggedIn,  async (req,res) => {
    const { id } = req.params;
    const {  nombre_categoria, descripcion_categoria, date_added} = req.body;
    const newHerramienta = {
        nombre_categoria,
        descripcion_categoria,
        //date_added,
        //id_categoria: req.user.id
    };
    //console.log(newLink);
    await pool.query('UPDATE categorias set ? WHERE id_categoria = ?', [newHerramienta, id]);
    req.flash('success', 'Se actualizo satisfactoriamente');
    res.redirect('/categoria');
});


//VISTA categoria/delte/:id
router.get('/delete/:id', isLoggedIn, async (req, res) => {
    const { id } = req.params;
    await pool.query('DELETE FROM categorias WHERE id_categoria = ?', [id]);
    req.flash('success','Se borro satisfactoriamente');
    res.redirect('/categoria');
});



module.exports = router;