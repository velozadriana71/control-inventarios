
const express = require('express');
const router = express.Router();
const passport = require('passport')
const {isLoggedIn} = require('../lib/auth')
const {isNotLoggedIn} = require('../lib/auth')
const pool = require('../database');
const helpers = require('../lib/helpers');


//renderizar el formulario
router.get('/usuario', async  (req,res) => {
    const usuario = await pool.query('SELECT * FROM users');
    res.render('usuarios/list.usuarios.hbs',{usuario})
});

router.get('/usuario/add', async  (req,res) => {
    const rol = await pool.query('SELECT * FROM rol');
    res.render('auth/signup',{rol})
});

router.post('/usuario/add', async (req,res) => {
        //async (req, username , password) =>{
        const { nombre, username, password, apellidos, fk_rol } = req.body;
        const newUser = {
            nombre,
            apellidos,
            username,
            password,
            fk_rol
        };
        //Cifrar contraseña
    newUser.password = await helpers.encryptPassword(password);
    await pool.query ('INSERT INTO users SET ?', [newUser]);
    
    newUser.id = result.insertId;
    req.flash('success','Guardador Correctamente');
    res.redirect('/herramienta');
});


//Recibir los datos
// router.post('/signup', (req,res) => {
//     console.log(req.body);
//     passport.authenticate('local.signup', {
//         successRedirect: '/profile',
//         failureRedirect: '/signup',
//         failureFlash: true
//     });
//     res.send('Recibido we');

// });


router.get('/signup', isNotLoggedIn, async (req,res) => {
    const rol = await pool.query('SELECT * FROM rol');
    res.render('auth/signup',{rol})
});


router.post('/signup', isNotLoggedIn, passport.authenticate('local.signup', {
    successRedirect: '/herramienta',
    failureRedirect: '/signup',
    failureFlash: true
}));

//Loguearse

router.get('/signin', isNotLoggedIn,  (req,res) => {
    res.render('auth/signin')
});

router.post('/signin', isNotLoggedIn, (req, res, next) => {
    passport.authenticate('local.signin', {
        successRedirect: '/herramienta',
        failureRedirect: '/signin',
        failureFlash: true
    })(req, res, next);
} );

//Registrarse
router.get('/profile', isLoggedIn, (req, res) => {
    res.render('profile')
});


router.get('/logout', isLoggedIn, (req, res) =>{
    req.logOut();
    res.redirect('/signin');
});

router.get('/delete/:id', isLoggedIn, async (req, res) => {
    const { id } = req.params;
    await pool.query('DELETE FROM users WHERE id = ?', [id]);
    req.flash('success','Se borro satisfactoriamente');
    res.redirect('/usuario');
});
module.exports = router;