
const { request } = require('express');
const express = require('express');
const router = express.Router();

const pool = require('../database');
const {isLoggedIn} = require ('../lib/auth');





router.get('/add', isLoggedIn, (req,res) => {
    res.render('links/add');
});

// router.get('/',  async (req, res) => {
//     const usuario = await pool.query('SELECT * FROM users');
//     console.log(usuario);
//     res.render('links/list.usuarios.hbs', {usuario});
// });


router.post('/add', isLoggedIn, async (req,res) => {
    const { title, url, description } = req.body;
    const newLink = {
        title,
        url,
        description,
        user_id: req.user.id
    };
    await pool.query('INSERT INTO link SET ?', [newLink]);
    req.flash('success','Guardador Correctamente');
    res.redirect('/links');
});

router.get('/', isLoggedIn,  async (req, res) => {
    const links = await pool.query('SELECT * FROM link WHERE user_id =?', [req.user.id]);
    console.log (links);
    res.render('links/list', { links });
});

router.get('/delete/:id', isLoggedIn, async (req, res) => {
    const { id } = req.params;
    await pool.query('DELETE FROM link WHERE ID = ?', [id]);
    req.flash('success','Se borro satisfactoriamente');
    res.redirect('/links');
});

router.get('/edit/:id', isLoggedIn, async (req, res) => {
    const { id } = req.params;
    const links = await pool.query('SELECT * FROM link WHERE id = ?', [id]);
    res.render('links/edit', {link: links[0]});
});

router.post('/edit/:id', isLoggedIn,  async (req,res) => {
    const { id } = req.params;
    const { title, description, url } = req.body;
    const newLink = {
        title,
        description,
        url
    };
    //console.log(newLink);
    await pool.query('UPDATE link set ? WHERE id = ?', [newLink, id]);
    req.flash('success', 'Se actualizo satisfactoriamente');
    res.redirect('/links');
});

module.exports = router;