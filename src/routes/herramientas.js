
const { request } = require('express');
const express = require('express');
const router = express.Router();
const Handlebars = require('handlebars');
const pool = require('../database');
const {isLoggedIn} = require ('../lib/auth');
const multer = require('multer');
const path = require('path')

//Multer
const storage = multer.diskStorage({
    destination: path.join(__dirname,'../public/img'),
    filename: (req, file, cb) =>{
        cb(null, file.originalname);
    }
});



//  router.get('/add', isLoggedIn, (req,res) => {
//      res.render('herramientas/add');
// });
    

//Vista /herramienta/add

router.get('/add', isLoggedIn, async (req,res) => {
    const categorias = await pool.query('SELECT id_categoria, nombre_categoria FROM categorias');
    console.log(categorias);
    console.log('Hola Mundo');
    res.render('herramientas/add',{categorias});
});


const subirImagen = multer({
    storage,
    dest : path.join(__dirname,'public/img')
}).single('imagen');



router.post('/add', isLoggedIn, subirImagen, async (req,res) => {
    // const { codigo_producto, nombre_producto, date_added, stock, fk_categoria, Image } = req.body;
    // const newHerramienta = {
    //     codigo_producto,
    //     nombre_producto,
    //     date_added,
    //     stock,
    //     fk_categoria,
    //     Image
    //     //id_producto: req.user.id
    // };
    const im = subirImagen.originalname;
    // const herramienta = await pool.query('INSERT INTO products SET ?', [newHerramienta]);
    // console.log(herramienta);
    console.log(req.file);
    console.log(im)
    req.flash('success','Guardador Correctamente');
    res.redirect('/herramienta');
});


//Vista /herramienta


     router.get('/', isLoggedIn, async (req, res) => {
        const { nombre_producto } = req.params;
         const herramienta = await pool.query('SELECT * FROM products');
         const categoria = await pool.query('SELECT id_categoria, nombre_categoria FROM categorias');
         const herramientabusqueda = await pool.query('SELECT * FROM products WHERE nombre_producto = ?',[nombre_producto]);
         res.render('herramientas/list.herramientas.hbs', {herramienta, categoria, herramientabusqueda});
        
  });

//   router.get('/', isLoggedIn, async (req, res) => {
//     const categoria = await pool.query('SELECT * FROM categorias');
//     console.log(categoria);
//     res.render('herramientas/list.herramientas.hbs', {categoria}); 
// });

// router.get('/', isLoggedIn,  async (req, res) => {
//     const herramienta = await pool.query('SELECT * FROM products WHERE id_producto =?', [req.user.id]);
//     //const categorias = await pool.query('SELECT * FROM categorias')
//     console.log (herramienta);
//     res.render('herramientas/list.herramientas.hbs', {herramienta});
// });



//Vista /herramienta/delete/

router.get('/delete/:id', isLoggedIn, async (req, res) => {
    const { id } = req.params;
    await pool.query('DELETE FROM products WHERE id_producto = ?', [id]);
    req.flash('success','Se borro satisfactoriamente');
    res.redirect('/herramienta');
});


//Vista /herramienta/stock/edit/
 router.get('/edit/:id', isLoggedIn, async (req, res) => {
     const { id } = req.params;
     const categorias = await pool.query('SELECT id_categoria, nombre_categoria FROM categorias')
     //const herramienta = await pool.query('SELECT * FROM products WHERE id_producto = ?', [id]);
     const herramienta = await pool.query('SELECT b.id_producto, b.nombre_producto, b.codigo_producto, b.stock, b.fk_categoria, a.nombre_categoria FROM categorias a, products b WHERE b.fk_categoria = a.id_categoria and id_producto = ?', [id]);
     console.log(herramienta);
     console.log(categorias);
     //console.log(categ);
     res.render('herramientas/edit.productos.hbs', {herramienta: herramienta[0], categorias});
});

router.post('/edit/:id', isLoggedIn,  async (req,res) => {
    const { id } = req.params;
    const { codigo_producto, nombre_producto, fk_categoria} = req.body;
    const newHerramienta = {
        codigo_producto,
        nombre_producto,
        fk_categoria
        //id_producto: req.user.id
    };
    console.log(newHerramienta); 
    res.send("bien");
    // await pool.query('UPDATE products set ? WHERE id_producto = ?', [newHerramienta, id]);
    // console.log(id);
    // console.log(newHerramienta);
    // req.flash('success', 'Se actualizo satisfactoriamente');
    // res.redirect('/herramienta');
});

//Vista herramienta/stock
router.get('/stock/:id', isLoggedIn, async (req, res) => {
    //const historial = await pool.query('SELECT * FROM historial');
    //res.render('herramientas/stock.hbs', {historial});
    const { id } = req.params;
    const herramienta = await pool.query('SELECT * FROM products WHERE id_producto = ?', [id]);
    const historial = await pool.query('SELECT a.id_historial, b.nombre_producto ,a.fecha, a.nota, b.stock FROM historial a, products b  where a.id_producto = b.id_producto and a.id_producto = ?', [id]);
    console.log(herramienta);
    res.render('herramientas/stock.hbs', {herramienta: herramienta[0], historial});
});

 router.get('/stock/entradas/:id', isLoggedIn, async (req, res) => {
     const { id } = req.params;
     const herramienta = await pool.query('SELECT * FROM products WHERE id_producto = ?', [id]);
    res.render('herramientas/entradas.hbs', {herramienta: herramienta[0]});
});

router.post('/stock/entradas/:id', isLoggedIn, async (req, res) => {
    const {id} = req.params;
    const { stock } = req.body;
    const newHerramienta = {
        stock,
        //id_producto: req.user.id
    };
    console.log(newHerramienta); 
    const herramienta = await pool.query('UPDATE products set ? + (SELECT stock FROM products where id_producto = ?) where id_producto = ?' , [newHerramienta, id, id]);
    
    const { id_producto, fecha, nota, cantidad } = req.body;
    const newHistorial ={
        nota
        //id_producto: req.user.id
    };
    console.log(newHistorial);
    const s =await pool.query('INSERT INTO historial set id_producto = ?, fecha= null, cantidad = 2, ?', [id, newHistorial]);
    console.log(s);
    req.flash('success', 'Se actualizo satisfactoriamente');
    res.redirect('/herramienta');
});




router.get('/stock/salidas/:id', isLoggedIn, async (req, res) => {
    const { id } = req.params;
    const herramienta = await pool.query('SELECT * FROM products WHERE id_producto = ?', [id]);
   res.render('herramientas/salidas.hbs', {herramienta: herramienta[0]});
});


router.post('/stock/salidas/:id', isLoggedIn, async (req, res) => {
    const {id} = req.params;
    const { stock } = req.body;
    const newHerramienta = {
        stock,
        //id_producto: req.user.id
    };
    console.log(newHerramienta); 
    const herramienta = await pool.query('UPDATE products set stock = 2 * (-1) + (SELECT stock FROM products where id_producto = 1) where id_producto = 1; ' , [newHerramienta, id, id]);
    //const herramienta = await pool.query('SELECT * From products Where id_producto =', id);
    console.log(id);
    req.flash('success', 'Se actualizo satisfactoriamente');
    res.redirect('/herramienta');
});




module.exports = router;