const passport = require('passport');
const express = require('express');
const router = express.Router();
const LocalStrategy = require('passport-local');
const pool = require('../database');
const helpers = require('../lib/helpers');

// //Login
// passport.use('local.signin', new LocalStrategy({
//     usernameField: 'username',
//     passwordField: 'password',
//     passReqToCallback: true
// }, async (req, username, password, done) => {
//     console.log(req.body)
//     const rows = await pool.query('SELECT * FROM users WHERE username = ?', [username])
    
//     if(rows.length > 0) {
//         const user = rows[0];
//         const validPassword = await helpers.matchPassword(password, user.password);
//         if  (validPassword)
//         {
//             done(null, user, req.flash('success','Bienvenido' + user.username));
//         } else {
//             done(null, false, req.flash('message','Contraseña incorrecta'));
//         }
//     }else {
//         return done(null, false, req.flash('message','El usuario no existe'));
//     }
// }));

// // router.get('/', isLoggedIn,  async (req, res) => {
// //     const user = await pool.query('SELECT * FROM users WHERE id =?', [id]);
// //     //console.log (links);
// //     res.render('usuarios/list.usuarios', { user });
// // });

// //Regitro
// passport.use('local.signup', new LocalStrategy ({
//     usernameField: 'username',
//     passwordField: 'password',
//     passReqToCallback: true
// }, async (req, username , password, done) =>{
//     const { nombre, apellidos, fk_rol } = req.body;
//     const newUser = {
//         username,
//         password,
//         nombre,
//         apellidos,
//         fk_rol
//     };
//     //Cifrar contraseña
//     newUser.password = await helpers.encryptPassword(password);
//     const result = await pool.query ('INSERT INTO users SET ?', [newUser]);
//     newUser.id = result.insertId;
//     //NewUser para que lo almacene en una session
//     return done(null, newUser);
// }));

// passport.serializeUser((user, done) =>{
//     done(null, user.id);
// });

// passport.deserializeUser( async (id, done) => {
//     const rows = await pool.query('SELECT * FROM users WHERE id = ?', [id]);
//     done(null, rows[0]);
// });


