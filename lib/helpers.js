const bcrypt = require ('bcryptjs')
const Handlebars = require ("handlebars")
const helpers = {};

    Handlebars.registerHelper('Nav', (a, b, opts) => {
    return a == b ? opts.fn(this) : opts.inverse(this);
    });

helpers.encryptPassword = async (password) => {
    const salt = await bcrypt.genSalt(10);
    const finalpass =await bcrypt.hash(password, salt);
    return finalpass;
};
//helpers.CompararContraseña
helpers.matchPassword = async (password, savedPassword) => {
    try {
        return await bcrypt.compare(password, savedPassword);
    }
    catch (e)
    {
        console.log(e);
    }
}

let fk_categoria = 1; // <- Deberías realizar la asignación dinámicamente, no en la vista HTML
handle.registerHelper('EsAdmin', (a, opts) => {
    return a == fk_categoria ? opts.fn(this) : opts.inverse(this);
});

handle.registerHelper('Nav', (a, b, opts) => {
    return a == b ? opts.fn(this) : opts.inverse(this);
});

module.exports = helpers;