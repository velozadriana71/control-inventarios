CREATE DATABASE Inventario2;

USE inventario2;

CREATE TABLE categorias (
  id_categoria int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  nombre_categoria varchar(255) NOT NULL,
  descripcion_categoria varchar(255) NOT NULL,
  date_added TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP not null
);

INSERT INTO categorias  VALUES
(1, 'Repuestos', 'Equipos para el hogar', '2021-12-19 00:00:00'),
(4, 'Equipos', 'Equipos stihl', '2021-12-19 21:06:37'),
(5, 'Accesorios', 'Accesorios stihl', '2021-12-19 21:06:39');

CREATE TABLE products (
  id_producto int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  codigo_producto char(20) NOT NULL UNIQUE KEY,
  nombre_producto char(255) NOT NULL,
  date_added TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP not null,
  stock int(11) NOT NULL,
  fk_categoria int(11) NOT NULL,
  FOREIGN KEY (fk_categoria) REFERENCES categorias (id_categoria)
);

INSERT INTO products VALUES (null,'her09','Martillo',null,5,1);

CREATE TABLE historial (
  id_historial int(11) NOT NULL PRIMARY KEY auto_increment,
  id_producto int(11) NOT NULL ,
  fecha TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP not null,
  nota varchar(255) NOT NULL,   
  cantidad int(11) NOT NULL,
  FOREIGN KEY (id_producto) REFERENCES products (id_producto)) ;
  
CREATE TABLE rol(
id_rol int(11) not null AUTO_INCREMENT PRIMARY KEY,
rol varchar(20) not null);

INSERT INTO rol VALUES (null,'Admin'), (null,'Encargado');

CREATE TABLE users (
  id int(11) Not null AUTO_INCREMENT PRIMARY key,
  username varchar(16) not null,
  password varchar(60) not null,
  nombre varchar(50) not null,
  apellidos varchar(50) not null,
  fk_rol int(11),
  FOREIGN KEY (fk_rol) REFERENCES rol (id_rol)
);

